#include "Ascii_Video.h"

static char pixel_to_char(cv::Vec3b &pixel) {
    int color = pixel[0];
    if (color <= 30) {
        return '@';
    } else if (color > 30 && color <= 55) {
        return '%';
    } else if (color > 55 && color <= 80) {
        return '#';
    } else if (color > 80 && color <= 105) {
        return '*';
    } else if (color > 105 && color <= 130) {
        return '+';
    } else if (color > 130 && color <= 155) {
        return '=';
    } else if (color > 155 && color <= 180) {
        return '-';
    } else if (color > 180 && color <= 205) {
        return ':';
    } else if (color > 205 && color <= 230) {
        return '.';
    } else {
        return ' ';
    }
}

Ascii_Video::Ascii_Video(std::string &filename_in, int target_height_in)
    : filename(filename_in), target_height(target_height_in) {
    outfile = "output.avi";
}

void Ascii_Video::set_outfile(std::string &outfile_in) {
    outfile = outfile_in;
}

void Ascii_Video::split_frames() {
    try {
        cv::VideoCapture raw_video(filename);
        if (!raw_video.isOpened()) {
            exit(1);
        }

        fourcc = static_cast<int>(raw_video.get(cv::CAP_PROP_FOURCC));
        framerate = raw_video.get(cv::CAP_PROP_FPS);

        for (int i = 0; i < raw_video.get(cv::CAP_PROP_FRAME_COUNT); ++i) {
            cv::Mat *frame = new cv::Mat;
            raw_video >> *frame;
            frames.push_back(frame);
        }
    } catch (...) {
        std::cerr << "Error opening file for reading: " << filename << std::endl;
        exit(1);
    }
}

void Ascii_Video::greyscale() {
    for (cv::Mat *frame : frames) {
        for (int i = 0; i < frame->rows; ++i) {
            for (int j = 0; j < frame->cols; ++j) {
                unsigned char &b = frame->data[frame->step * i + j*frame->channels()];
                unsigned char &g = frame->data[frame->step * i + j*frame->channels() + 1];
                unsigned char &r = frame->data[frame->step * i + j*frame->channels() + 2];

                int color = 0.07*b+0.72*g+0.21*r;

                b = color;
                g = color;
                r = color;
            }
        }
    }
}

void Ascii_Video::asciify() {
    for (cv::Mat *frame : frames) {
        Frame *ascii_frame = new Frame;
        ascii_frame->width = frame->cols;
        ascii_frame->height = frame->rows;

        for (int i = 0; i < frame->rows; ++i) {
            for (int j = 0; j < frame->cols; ++j) {
                cv::Vec3b pixel;
                pixel[0] = frame->data[frame->step * i + j*frame->channels()];
                pixel[1] = frame->data[frame->step * i + j*frame->channels() + 1];
                pixel[2] = frame->data[frame->step * i + j*frame->channels() + 2];

                ascii_frame->pixels.push_back(pixel_to_char(pixel));
            }
        }

        ascii_frames.push_back(ascii_frame);

        delete frame;
    }
}

void Ascii_Video::draw_frames() {
    cv::Ptr<cv::freetype::FreeType2> font = cv::freetype::createFreeType2();
    font->loadFontData("./square.ttf", 0);
    int font_size = 12;

    double scaling_factor = ascii_frames[0]->height * font_size / target_height;

    for (int frame_num = 0; frame_num < ascii_frames.size(); ++frame_num) {
        cv::Mat *ascii_image = new cv::Mat(ascii_frames[frame_num]->height * font_size / scaling_factor, ascii_frames[frame_num]->width * font_size / scaling_factor, CV_8UC3, cv::Scalar(255, 255, 255));

        for (int i = 0; i < ascii_frames[frame_num]->height; i += scaling_factor) {
            std::string row;
            for (int j = 0; j < ascii_frames[frame_num]->width; j += scaling_factor) {
                row += ascii_frames[frame_num]->pixels[i * ascii_frames[frame_num]->width + j];
            }

            font->putText(*ascii_image, row, cv::Point(0, ceil(i/scaling_factor) * font_size + font_size), font_size, CV_RGB(0, 0, 0), -1, 8, true);
        }

        ascii_images.push_back(ascii_image);

        delete ascii_frames[frame_num];
    }
}

void Ascii_Video::save() {
    cv::VideoWriter writer;
    writer.open(outfile, fourcc, framerate, ascii_images[0]->size(), true);
    if (!writer.isOpened()) {
        std::cerr << "Error opening file for writing: " << outfile << std::endl;
        exit(1);
    }

    for (cv::Mat *frame : ascii_images) {
        writer << *frame;
        
        delete frame;
    }
}
