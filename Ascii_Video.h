#ifndef ASCII_VIDEO_H
#define ASCII_VIDEO_H

#include <iostream>
#include <math.h>
#include <string>
#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/freetype.hpp>
#include <vector>

class Ascii_Video {
    private:
        class Frame {
            public:
                int width;
                int height;
                std::vector<char> pixels;
        };

        std::string filename;
        std::string outfile;
        int target_height;
        std::vector<cv::Mat *> frames;
        std::vector<Frame *> ascii_frames;
        std::vector<cv::Mat *> ascii_images;
        int fourcc;
        double framerate;
    public:
        Ascii_Video(std::string &filename_in, int target_height_in);

        void set_outfile(std::string &outfile_in);

        void split_frames();

        void greyscale();

        void asciify();

        void draw_frames();

        void save();
};

#endif
