#include <getopt.h>
#include <iostream>
#include <string>
#include "Ascii_Video.h"

void process_video(Ascii_Video &asciifier) {
    asciifier.split_frames();
    asciifier.greyscale();
    asciifier.asciify();
    asciifier.draw_frames();
    asciifier.save();
}

void usage() {
    std::cerr << "Usage: AsciiVideo++.exe -i input_file -h output_height [-o output_file]" << std::endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    std::string filename = "";
    std::string outfile = "";
    int target_height = -1;;

    int opt;
    while ((opt = getopt(argc, argv, "i:o:h:")) != -1) {
        switch (opt) {
            case 'i':
                filename = optarg;
                continue;
            case 'o':
                outfile = optarg;
                continue;
            case 'h':
                target_height = atoi(optarg);
                continue;
            default:
                break;
        }
    }

    if (filename == "") {
        usage();
    }

    if (target_height == -1) {
        usage();
    }

    Ascii_Video asciifier(filename, target_height);

    if (outfile != "") {
        asciifier.set_outfile(outfile);
    }

    process_video(asciifier);

    exit(0);
}
