release: main.cpp Ascii_Video.cpp Ascii_Video.h
	g++ -o AsciiVideo++.exe `pkg-config --cflags opencv4` main.cpp Ascii_Video.cpp `pkg-config --libs opencv4`

debug: main.cpp Ascii_Video.cpp Ascii_Video.h
	g++ -g -o AsciiVideo++.exe `pkg-config --cflags opencv4` main.cpp Ascii_Video.cpp `pkg-config --libs opencv4`