# AsciiVideo++

A command line program capable of replacing every frame of a video with an ASCII art representation
of those frames, like in this example: 

![Original](https://i.imgur.com/qw8xqnJ.gif)
![After AsciiVideo++](https://i.imgur.com/qjPrkLo.gif)

## Credits

Square font courtesy of [Wouter van Oortmerssen](http://strlen.com/square/).

Makes use of [OpenCV](https://opencv.org/).

Relevant licenses for the above can be found in the repo's "Licenses" folder.

Color scale adapted from [Paul Bourke](http://paulbourke.net/dataformats/asciiart/).

Dog GIF from [Reddit](https://www.reddit.com/r/gifs/comments/9ee31i/wiggle_wiggle_p/).